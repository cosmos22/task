from django.utils.decorators import method_decorator
from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import action
from rest_framework.response import Response

from users.models import User
from users.serializers import UserSerializer
from utils.permissions import OwnerReadOnly
from utils.viewsets import RetrieveModelViewSet


@method_decorator(
    name="retrieve",
    decorator=swagger_auto_schema(operation_description="사용자 본인 정보 조회(마이페이지)"),
)
class UserViewSet(RetrieveModelViewSet):
    queryset = User.objects.all()
    serializer_classes = {
        "retrieve": UserSerializer,
        "follow": UserSerializer,
        "unfollow": UserSerializer,
    }
    permission_classes = [OwnerReadOnly]

    def get_queryset(self):
        return self.queryset.prefetch_related("following_set", "follower_set").all()

    @swagger_auto_schema(operation_description="특정 사용자를 팔로우한다")
    @action(methods=["POST"], detail=True)
    def follow(self, request, pk):
        user = self.request.user
        user.follow_user(pk)
        serializer = self.get_serializer(user)

        return Response(serializer.data)

    @swagger_auto_schema(operation_description="특정 사용자를 언팔로우한다")
    @action(methods=["POST"], detail=True)
    def unfollow(self, request, pk):
        user = self.request.user
        user.unfollow_user(pk)
        serializer = self.get_serializer(user)

        return Response(serializer.data)
