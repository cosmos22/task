from drf_yasg.utils import swagger_serializer_method
from rest_framework import serializers

from users.models import User


class FollowUser(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            "id",
            "name",
            "anonymized_email",
        )


class UserSerializer(serializers.ModelSerializer):
    followers = serializers.SerializerMethodField(read_only=True)
    followings = serializers.SerializerMethodField(read_only=True)

    @swagger_serializer_method(serializer_or_field=FollowUser)
    def get_followers(self, obj):
        follower_list = []
        for follower in obj.follower_set.all():
            follower_list.append(follower.follower)
        return FollowUser(follower_list, many=True).data

    @swagger_serializer_method(serializer_or_field=FollowUser)
    def get_followings(self, obj):
        following_list = []
        for following in obj.following_set.all():
            following_list.append(following.following)
        return FollowUser(following_list, many=True).data

    class Meta:
        model = User
        fields = (
            "id",
            "email",
            "name",
            "following_count",
            "follower_count",
            "followers",
            "followings",
        )
