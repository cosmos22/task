from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.db import models, transaction
from rest_framework.exceptions import APIException
from safedelete.managers import SafeDeleteManager
from utils.models import DefaultModel


class UserFollow(models.Model):
    follower = models.ForeignKey(
        "User", related_name="following_set", on_delete=models.CASCADE
    )
    following = models.ForeignKey(
        "User", related_name="follower_set", on_delete=models.CASCADE
    )


class UserManager(SafeDeleteManager, BaseUserManager):
    def create_user(
        self, email, name, password=None, is_superuser=False, **extra_fields
    ):
        user = self.model(
            email=self.normalize_email(email),
            name=name,
            is_superuser=is_superuser,
            **extra_fields,
        )

        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, name, password):
        """
        SuperUser 생성시 사용
        """
        return self.create_user(
            email, name, password=password, is_superuser=True, is_staff=True
        )


class User(DefaultModel, PermissionsMixin, AbstractBaseUser):
    email = models.EmailField("이메일", max_length=256, unique=True)
    name = models.CharField("이름", max_length=100, blank=True, null=True)
    is_staff = models.BooleanField("관리자여부", default=False)

    USERNAME_FIELD = "email"

    objects = UserManager()

    def __str__(self):
        return f"[{self.id}] {self.email}"

    @property
    def anonymized_email(self):
        front, domain = self.email.split("@")
        anonymized = "".join(
            [char if idx == 0 else "*" for idx, char in enumerate(front)]
        )
        return f"{anonymized}@{domain}"

    @property
    def following_count(self):
        return self.followers.count()

    @property
    def follower_count(self):
        return self.followings.count()

    @transaction.atomic
    def follow_user(self, user_id):
        try:
            if self.id == user_id:
                raise APIException("자기 자신은 팔로우할 수 없습니다.")

            follower = User.objects.get(id=user_id)
            UserFollow.objects.update_or_create(follower=follower, following=self)
        except User.DoesNotExist:
            raise APIException("user id에 맞는 사용자가 없습니다.")

    @transaction.atomic
    def unfollow_user(self, user_id):
        try:
            if self.id == user_id:
                raise APIException("자기 자신은 언팔로우할 수 없습니다.")

            follower = User.objects.get(id=user_id)
            user_follow = UserFollow.objects.get(follower=follower, following=self)
            user_follow.delete()
        except User.DoesNotExist:
            raise APIException("user id에 맞는 사용자가 없습니다.")
