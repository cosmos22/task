from rest_framework.permissions import IsAdminUser

from staff.users.serializers import UserSerializer
from users.models import User
from utils.viewsets import ModelViewSet


class UserViewSet(ModelViewSet):
    queryset = User.objects.all()
    serializer_classes = {
        "create": UserSerializer,
        "update": UserSerializer,
        "list": UserSerializer,
        "retrieve": UserSerializer,
        "destroy": UserSerializer,
    }
    permission_classes = [IsAdminUser]
