from rest_framework.permissions import IsAuthenticated


class OwnerReadOnly(IsAuthenticated):
    def has_object_permission(self, request, view, obj):
        return bool(request.user.is_staff or obj == request.user)
