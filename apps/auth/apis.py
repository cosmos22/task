from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework_simplejwt.views import TokenViewBase

from auth.serializers import (
    LoginSerializer,
    RegisterSerializer,
    TokenSerializer,
    LogoutSerializer,
)
from users.models import User
from utils.viewsets import CreateModelViewSet


class AuthViewSet(TokenViewBase, CreateModelViewSet):
    queryset = User.objects.all()
    serializer_classes = {
        "login": LoginSerializer,
        "register": RegisterSerializer,
        "logout": LogoutSerializer,
    }
    permission_classes = [AllowAny]

    @swagger_auto_schema(auto_schema=None)
    def create(self, request, *args, **kwargs):
        pass

    @swagger_auto_schema(
        operation_description="로그인", responses={status.HTTP_200_OK: TokenSerializer}
    )
    @action(methods=["POST"], detail=False)
    def login(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)

    @swagger_auto_schema(operation_description="회원가입")
    @action(methods=["POST"], detail=False)
    def register(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @swagger_auto_schema(
        operation_description="로그아웃. refresh token 블랙리스트에 추가. 클라이언트에서 토큰 폐기",
        responses={status.HTTP_200_OK: "No Response body"},
    )
    @action(methods=["POST"], detail=False)
    def logout(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)
