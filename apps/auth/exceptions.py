from rest_framework import status
from rest_framework.exceptions import APIException


class TokenNotValid(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = "토큰을 확인해주세요."
