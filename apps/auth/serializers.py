from django.contrib.auth.password_validation import validate_password
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from rest_framework_simplejwt.serializers import (
    TokenObtainPairSerializer as DefaultTokenObtainPairSerializer,
)
from rest_framework_simplejwt.tokens import RefreshToken

from users.models import User
from auth.exceptions import TokenNotValid


class TokenSerializer(serializers.Serializer):
    refresh = serializers.CharField(read_only=True, help_text="리프래시 토큰")
    access = serializers.CharField(read_only=True, help_text="엑세스 토큰")


class LoginSerializer(DefaultTokenObtainPairSerializer):
    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)

        # custom claims
        token["email"] = user.anonymized_email
        token["name"] = user.name
        token["email"] = user.anonymized_email
        return token


class RegisterSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
        required=True, validators=[UniqueValidator(queryset=User.objects.all())]
    )
    password = serializers.CharField(
        write_only=True, required=True, validators=[validate_password]
    )
    password_2 = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = User
        fields = (
            "email",
            "name",
            "password",
            "password_2",
        )

    def validate(self, attrs):
        if attrs["password"] != attrs["password_2"]:
            raise serializers.ValidationError({"password": "Check password"})

        return attrs

    def create(self, validated_data):
        user = User.objects.create(
            email=validated_data["email"],
            name=validated_data["name"],
        )

        user.set_password(validated_data["password"])
        user.save()

        return user


class LogoutSerializer(serializers.Serializer):
    refresh = serializers.CharField(help_text="리프래시 토큰")
    access = serializers.CharField(help_text="엑세스 토큰")

    def save(self):
        try:
            token = self.validated_data.get("refresh")
            token = RefreshToken(token)
            token.blacklist()
        except Exception as e:
            raise TokenNotValid()

    def to_representation(self, instance):
        return {}
