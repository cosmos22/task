# db
```shell
$ docker-compose up -d
```

# start server
```shell
$ ./manage.py runserver
```

# docs
```shell
http://localhost:8000/docs/
```